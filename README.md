# App Api

Fixed API for visual applications. It is usually used for the internal needs of other libraries

[Surface](src%2FcommonMain%2Fkotlin%2Fcasperix%2Fapp%2Fsurface%2FSurface.kt) - the basic interface of the application window.

## deploy

### Local
Just add water:
- `./gradlew publishToMavenLocal`

### Nexus
Need variables (ask the administrator):
`ossrhUsername`,
`ossrhPassowrd`,
`signing.keyId`,
`signing.password`,
`signing.secretKeyRingFile`.


Publishing, closing, releasing:
- `./gradlew publishToSonatype`
- `./gradlew findSonatypeStagingRepository closeAndReleaseSonatypeStagingRepository`

In case of troubles, see also:
https://github.com/gradle-nexus/publish-plugin