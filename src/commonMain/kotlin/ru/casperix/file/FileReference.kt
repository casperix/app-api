package ru.casperix.file

data class FileReference(val type: FilePathType, val path: String) {
	companion object {
		fun classpath(path: String): FileReference {
			return FileReference(FilePathType.CLASSPATH, path)
		}

		fun external(path: String): FileReference {
			return FileReference(FilePathType.EXTERNAL, path)
		}

		fun internal(path: String): FileReference {
			return FileReference(FilePathType.INTERNAL, path)
		}

		fun absolute(path: String): FileReference {
			return FileReference(FilePathType.ABSOLUTE, path)
		}
	}
}