package ru.casperix.file

enum class FilePathType {
	/**
	 * Path to application resources (readonly)
	 */
	CLASSPATH,

	/**
	 * 	Path to self application storage (writable)
	 */
	EXTERNAL,

	/**
	 * 	Path to application assets (readonly)
	 * 	May be different from CLASSPATH
	 */
	INTERNAL,

	/**
	 * 	Path in file system
	 */
	ABSOLUTE
}