package ru.casperix.app.window

import ru.casperix.math.array.int32.IntMap2D
import ru.casperix.math.vector.int32.Vector2i


interface Cursor


enum class SystemCursor : Cursor {
    DEFAULT,
    HAND,
    TEXT,
    WAIT,
    MOVE,
}

class BitmapCursor(val image: IntMap2D, val hotSpot: Vector2i) : Cursor

interface CursorSupport {
    fun setCursor(value: Cursor)
    fun getCursor(): Cursor
}