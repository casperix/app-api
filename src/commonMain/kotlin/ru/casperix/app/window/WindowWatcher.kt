package ru.casperix.app.window

import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.signals.concrete.EmptyPromise
import ru.casperix.signals.concrete.Promise
import ru.casperix.signals.concrete.StoragePromise

/**
 * 	Abstract proxy for application
 */
@Deprecated(message = "Use Surface")
interface WindowWatcher {
    val onCursor: StoragePromise<Cursor>
    val onResize: StoragePromise<Dimension2i>
    val onUpdate: Promise<Double>
    val onPreRender: EmptyPromise
    val onRender: EmptyPromise
    val onPostRender: EmptyPromise
    val onExit: EmptyPromise
}

