package ru.casperix.app.surface

import ru.casperix.app.surface.component.FullScreenSupport
import ru.casperix.app.surface.component.ResizeSupport
import ru.casperix.app.window.CursorSupport

interface DesktopWindow : Surface, ResizeSupport, FullScreenSupport, CursorSupport