package ru.casperix.app.surface

import ru.casperix.input.InputEvent
import ru.casperix.misc.Disposable
import kotlin.time.Duration

interface SurfaceListener : Disposable {
    fun input(event: InputEvent)
    fun nextFrame(tick: Duration)
}


