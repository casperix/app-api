package ru.casperix.app.surface

import ru.casperix.app.surface.component.VirtualKeyboardApi

interface AndroidSurface : Surface, VirtualKeyboardApi