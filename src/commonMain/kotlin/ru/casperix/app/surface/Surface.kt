package ru.casperix.app.surface

import ru.casperix.app.surface.component.Display
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.vector.int32.Vector2i

interface Surface {
    fun requestFrame()
    fun getDisplay(): Display
    fun setTitle(value: String)
    fun getTitle(): String

    fun getVerticalSync(): Boolean
    fun setVerticalSync(value: Boolean)

    fun getSize(): Dimension2i
    fun getPosition(): Vector2i

    fun getTypeName(): String {
        return if (this is AndroidSurface) {
            "Android"
        } else if (this is WebPage) {
            "WebPage"
        } else if (this is DesktopWindow) {
            "Desktop"
        } else {
            "Unknown"
        }
    }
}