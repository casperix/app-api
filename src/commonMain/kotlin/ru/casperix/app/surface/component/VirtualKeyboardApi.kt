package ru.casperix.app.surface.component

interface VirtualKeyboardApi {
    fun showVirtualKeyboard()
    fun hideVirtualKeyboard()
}