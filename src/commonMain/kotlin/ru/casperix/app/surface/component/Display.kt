package ru.casperix.app.surface.component

import ru.casperix.math.axis_aligned.float32.Dimension2f
import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.int32.Vector2i

class Display(
    val position: Vector2i,
    val resolution: Dimension2i,
    val inchSize: Dimension2f,
    val bitsPerPixel: Int,
    val frequency: Int,
) {
    val displayArea get() =  Box2i.byDimension(position, resolution.toVector2i())
    val densityPerInch get() = Vector2f( resolution.width / inchSize.width , resolution.height / inchSize.height)
}