package ru.casperix.app.surface.component

import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.vector.int32.Vector2i

interface ResizeSupport  {
    fun maximize()
    fun restore()

    fun minimize()

    fun setSize(value: Dimension2i)
    fun setPosition(value: Vector2i)

}

