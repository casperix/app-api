package ru.casperix.app.surface

import ru.casperix.app.surface.component.FullScreenSupport
import ru.casperix.app.window.CursorSupport

interface WebPage : Surface, FullScreenSupport, CursorSupport